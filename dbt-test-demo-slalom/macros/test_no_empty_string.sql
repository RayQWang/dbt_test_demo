{% macro test_no_empty_string(model, column_name) %}
with validation as (
    select
        {{ column_name }} as field_to_test
    from {{ model }}
),
validation_errors as (
    select
        field_to_test
    from validation
    where field_to_test = ''
)
select *
from validation_errors
{% endmacro %}