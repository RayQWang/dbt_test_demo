select f1.buyerid, f1.saletime  
from fact_sales f1 
inner join 
    (select buyerid, max(saletime) as last_purchase_time  from fact_sales group by buyerid) fm
    on f1.buyerid = fm.buyerid and f1.saletime = fm.last_purchase_time 
order by f1.buyerid 