select buyerid, last_purchase_time from (
        select buyerid, saletime as last_purchase_time,
        rank() over (partition by buyerid order by saletime desc ) as r
        from fact_sales
    )
where r = 1